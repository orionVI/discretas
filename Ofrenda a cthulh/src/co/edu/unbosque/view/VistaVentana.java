package co.edu.unbosque.view;

import javax.swing.JOptionPane;

public class VistaVentana {

    public VistaVentana() {

    }

    public int leerDato(String mensaje) {
        int dato = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
        return dato;
    }

    public void mostrarInformacion(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }
}